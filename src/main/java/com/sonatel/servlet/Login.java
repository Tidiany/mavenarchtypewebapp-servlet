package com.sonatel.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet("/login")
public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*super.doGet(req, resp);*/

        String login = req.getParameter("txtLogin");
        String password = req.getParameter("txtPassword");
        if (login == null) {
            login = "";
        }
        if (password == null) {
            password = "";
        }

        resp.setContentType("text/html");
        try (PrintWriter out = resp.getWriter()){
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("   <head>");
            out.println("       <title> Authentification </title>");
            out.println("       <link rel='stylesheet' type='text/css' href='style.css'/>");
            out.println("   </head>");
            out.println("   <body>");
            out.println("       <div id='authentification-form'>");
            out.println("           <h1> Authentification </h1>");
//            out.println("           <h2> "+ LocalDate.now() +" </h2>");
            out.println("           <form method='post'>");
            out.println("               <label for='txtLogin'> Login : </label>");
            out.println("               <input type='text' id='txtLogin' name='txtLogin' value='" + login + "'/> <br/>");
            out.println("               <br/>");
            out.println("               <label for='txtPassword'> Password : </label>");
            out.println("               <input type='password' id='txtPassword' name='txtPassword' value='" + password + "'/> <br/>");
            out.println("               <br/>");
            out.println("               <button type='submit' class='submit' name='btnConnect'>Connecter</button>");
            out.println("           </form>");
            out.println("       </div>");
            out.println("   </body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("txtLogin");
        String password = req.getParameter("txtPassword");

        if (login.equals("Tidiany") && password.equals("Test337")) {
            resp.setContentType("text/html");
            try (PrintWriter out = resp.getWriter()){
                out.println("OK");
            }
        }else {
            doGet(req, resp);
        }
    }
}
